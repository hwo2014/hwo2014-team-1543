require_relative 'act'

# Debug-mode:
# true: Print many info on the command line or terminal. Crash on error.
# false: No info on the command line. No crash on error.
DEBUG_MODE = false
# VISUAL = print descriptive messages to the standard output
# LOG = print data logs to the standard output
# Visual messages and data logs are printed to the same output - visuals
# should be turned off when gathering data as they might be a problem when
# parsing the printed logs
VISUAL     = true 
LOG        = false
LOG_DIR    = "../data/raw"



MIN_THROTTLE               = 0.0
DEFAULT_THROTTLE           = 0.60
MAX_THROTTLE               = 1.0
DRIFT_TEST_THROTTLE_VALUES = [ 0.63,
                               0.62,
                               0.61,
                               0.59 ]
DRIFT_TEST_MIN_START_SPEED = 4
ACCELERATION_TEST_THROTTLE = 1.0
ACCELERATION_TEST_MAX_START_SPEED = 4
FRICTION_TEST_MIN_START_SPEED = 4
FRICTION_TEST_THROTTLE     = 0.0

TESTS = [ ProjectD::TestAccelerationAct,
#          ProjectD::TestFrictionAct,
#          ProjectD::TestMaxAngleAct,
          #ProjectD::TestDriftAct
            ]
