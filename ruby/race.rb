module ProjectD
    class Race
        attr_accessor :cars, :track, :session, :states, :max_angle

        def initialize(cars, track, laps, max_lap_time, quick_race, states = [])
            # Cars is a hash with car ids as keys. car ids are hashes that are
            # same format as car ids in jsons from the server
            @cars   = cars
            @track  = track
            @laps   = laps
            @max_lap_time = max_lap_time
            @quick_race = quick_race
            @states = states
            @max_angle = 0
        end

        def next_car_distance( car_id, lane = nil)
            dist = 100000
            next_car = nil
            state = @states[-1]
            car_states = state.car_states
            pieces = @track.pieces
            current_piece = car_states[car_id].piece
            current_piece_d = car_states[car_id].piece_d
            next_car_id = nil
            if lane.nil?
                lane = car_states[car_id].end_lane
            end
            car_states.each do |id, car_state|
                if car_state == car_states[car_id] ||
                    car_state.end_lane != car_states[car_id].end_lane
                    next
                end
                d = (((car_state.piece.index - current_piece.index) +
                    pieces.length) % pieces.length) * 1000
                d += car_state.piece_d - current_piece_d
                if d < 0
                    d+= pieces.length * 1000
                end
                if d < dist
                    next_car_id = id
                end
            end
            
            if next_car_id.nil?
                return pieces.length * 1000
            end

            next_car_state = car_states[next_car_id]
            next_car_piece_i = next_car_state.piece.index
            next_car_piece_d = next_car_state.piece_d
            
            dist = -current_piece_d
            lane = car_states[car_id].end_lane
            Range.new(current_piece.index, next_car_piece_i).each do |i|
                dist += pieces[i].length(lane)
            end
            dist += next_car_piece_d
            return dist
        end
        
        def next_switch( car_id, current_piece_i = nil)
            state      = @states[-1]
            car_states = state.car_states
            car_state  = car_states[car_id]
            if current_piece_i.nil?
                current_piece_i = car_state.piece.index
            end
            pieces     = @track.pieces
            piece      = pieces[current_piece_i]
            i          = piece.index
            while true do
                i += 1
                i %= pieces.length
                piece_tmp = pieces[i]
                if piece_tmp.switch
                     return piece_tmp
                end
            end
        end
        
        def next_bend( car_id, current_piece_i = nil)
            state      = @states[-1]
            car_states = state.car_states
            car_state  = car_states[car_id]
            if current_piece_i.nil?
                current_piece_i = car_state.piece.index
            end
            pieces     = @track.pieces
            piece      = pieces[current_piece_i]
            i          = piece.index
            while true do
                i += 1
                i %= pieces.length
                piece_tmp = pieces[i]
                if piece_tmp.class == BendPiece
                     return piece_tmp
                end
            end
        end
        
        def distance_to_piece( car_id, lane, to_piece,
                               current_piece_i = nil,
                               current_piece_d = nil)
            state      = @states[-1]
            car_states = state.car_states
            car_state  = car_states[car_id]
            if current_piece_i.nil? || current_piece_d.nil?
                current_piece_i = car_state.piece.index
                current_piece_d = car_state.piece_d
            end
            pieces     = @track.pieces
            piece      = pieces[current_piece_i]
            piece_d    = current_piece_d
            d          = piece.length(lane) - piece_d
            i          = piece.index
            while true do
                i += 1
                i %= pieces.length
                piece_tmp = pieces[i]
                if piece_tmp == to_piece
                     return d
                else
                    d += piece_tmp.length(lane)
                end
            end
            return d
        end
        
        def distance_to_next_bend( car_id, lane,
                                   current_piece_i = nil, current_piece_d = nil)
            state      = @states[-1]
            car_states = state.car_states
            car_state  = car_states[car_id]
            if current_piece_i.nil? || current_piece_d.nil?
                current_piece_i = car_state.piece.index
                current_piece_d = car_state.piece_d
            end
            pieces     = @track.pieces
            piece      = pieces[current_piece_i]
            piece_d    = current_piece_d
            d          = piece.length(lane) - piece_d
            i          = piece.index
            while true do
                i += 1
                i %= pieces.length
                piece_tmp = pieces[i]
                if piece_tmp.class == BendPiece
                    return d
                else
                    d += piece_tmp.length(lane)
                end
            end
            return d
        end
        
        def distance_to_next_switch( car_id, lane,
                                     current_piece_i = nil, current_piece_d = nil)
            state      = @states[-1]
            car_states = state.car_states
            car_state  = car_states[car_id]
            if current_piece_i.nil? || current_piece_d.nil?
                current_piece_i = car_state.piece.index
                current_piece_d = car_state.piece_d
            end
            pieces     = @track.pieces
            piece      = pieces[current_piece_i]
            piece_d    = current_piece_d
            d          = piece.length(lane) - piece_d
            i          = piece.index
            while true do
                i += 1
                i %= pieces.length
                piece_tmp = pieces[i]
                if piece_tmp.switch
                    return d
                else
                    d += piece_tmp.length(lane)
                end
            end
        end
        
        def update_state(race_state)
            if @states.length > 0 && race_state.tick == states.last.tick
                puts_debug('Ignoring duplicate state')
                return
            end
            # Calculate some extra state data
            if(@states.length > 0)
                fill_data(race_state)
            end
            if(@states.length > 2)
                fill_data2(race_state)
            end
            # Set as current state
            @states.push(race_state)

        end

        def fill_data(race_state)
            last_state = @states.last
            @cars.each_value do |car|
                id  = car.id_hash
                new = race_state.car_states[id]
                if !car.crashed.nil? then new.crashed  = car.crashed end
                if !car.throttle.nil? then new.throttle = car.throttle end
            end

        end

        def fill_data2(new_state)
            states = [ @states[-2],
                       @states[-1],
                       new_state]
            @cars.each_value do |car|
                id = Hash['name' => car.name, 'color' => car.color]
                car_states = states.collect { |state| state.car_states[id] }
                pieces     = car_states.collect { |car_state| car_state.piece }
                ticks      = states.collect { |state| state.tick }
                piece_ds   = car_states.collect { |s| s.piece_d } 
                piece_is   = car_states.collect { |s| s.piece.index }
                angles     = car_states.collect { |s| s.angle }
                lanes      = car_states.collect { |s| s.end_lane }
                speeds     = car_states.collect { |s| s.speed }
                angle_speeds = car_states.collect { |s| s.angle_speed }
                tan_speeds = car_states.collect { |s| s.tan_speed }
                dist = (pieces[2] == pieces[0]) ?
                    piece_ds[2] - piece_ds[0] :
                    pieces[0].length(lanes[0]) - piece_ds[0] + piece_ds[2]
                angle_d = angles[2] - angles[0]
                speed              = dist / (ticks[2] - ticks[0])
                angle_speed        = angle_d / (ticks[2] - ticks[0])
                tan_speed          = Math.sin((angles[1] / 180) * Math::PI) *
                                     speed
                acceleration       = (speed - speeds[0]) / (ticks[1] - ticks[0])
                angle_acceleration = (angle_speed - angle_speeds[0]) /
                                         (ticks[1] - ticks[0])
                tan_acceleration   = (tan_speed - tan_speeds[0]) /
                                         (ticks[1] - ticks[0])
                car_states[1].speed        = speed
                car_states[1].acceleration = acceleration
                car_states[1].angle_speed  = angle_speed
                car_states[1].tan_speed    = tan_speed
                car_states[1].angle_acceleration = angle_acceleration
                car_states[1].tan_acceleration = tan_acceleration


                dist = (pieces[2] == pieces[1]) ?
                    piece_ds[2] - piece_ds[1] :
                    pieces[1].length(lanes[1]) - piece_ds[1] + piece_ds[2] 
                angle_d = angles[2] - angles[1]
                speed              = dist / (ticks[2] - ticks[1])
                angle_speed        = angle_d / (ticks[2] - ticks[1])
                acc                = (speed - speeds[1]) /
                                         (ticks[2] - ticks[1])
                angle_acceleration = (angle_speed - angle_speeds[1]) /
                                         (ticks[2] - ticks[1])
                car_states[2].speed        = speed
                car_states[2].angle_speed  = angle_speed
                car_states[2].acceleration = acc
                car_states[2].angle_acceleration = angle_acceleration
            end
        end
        
        def update_track_max_angle(angle)
            @track.max_angle = angle
            puts_visual('Max angle updated: ' + angle.to_s)
            if(!@track.drift_angle_func.nil?)
                a_spd = @track.drift_angle_func.getX(@track.max_angle)
                update_track_max_angular_speed(a_spd)
            end
        end
        
        def update_track_max_angular_speed(s_spd)
            puts_visual('Max angular speed updated: ' + a_spd.to_s)
            @track.max_angular_speed = a_spd
        end

        def update_track_acceleration_function(func)
            @track.acceleration_func = func
            puts_visual('Acceleration values updated: acc = ' +
                        func.a.to_s + ' * spd + ' + func.b.to_s)
        end

        # Friction is included in the
        #   acceleration = acceleration_func(throttle, speed)
        # which can give negative values
        #def update_track_friction_function(func)
        #    @track.friction_func = func
        #    puts_visual('Friction values updated: fric = ' +
        #                func.a.to_s + ' * spd + ' + func.b.to_s)
        #end

        # Still figuring out the drifting algorithm
        #def update_track_drift_angle_function(func)
        #    @track.drift_angle_func = func
        #    puts_visual('Drift values updates: angle = ' +
        #                func.a.to_s + " * cos(angle) * angular_vel + " +
        #                func.b.to_s)
        #    if(!@track.max_angle.nil?)
        #        max_a_speed = func.getX(@track.max_angle)
        #        @track.max_angular_speed = max_a_speed
        #        puts_visual('Max angular speed updated: ' + max_a_speed.to_s)
        #    end
        #end

        def crashed(car_id, tick) 
            last_state = @states.last
            if last_state.tick >= tick
                last_state.car_states[car_id].crashed = true
            end
            @cars[car_id].crashed = true
            new_max_angle = @track.max_angle
            crash_angle = nil
            car_state_1 = @states[-1].car_states[car_id]
            car_state_2 = @states[-2].car_states[car_id]
            if !car_state_1.crashed
                crash_angle = car_state_1.angle.abs
            elsif !car_state_2.crashed
                crash_angle = car_state_2.angle.abs
            end
            if !crash_angle.nil? &&
                    (@track.max_angle.nil? || crash_angle < @track.max_angle)
                update_track_max_angle(crash_angle)
            end
        end
        
        def spawned(car_id, tick) 
            last_state = @states.last
            if last_state.tick >= tick
                last_state.car_states[car_id].crashed = false 
            end
            @cars[car_id].crashed = false
        end

        def to_hash
            cars_hash    = @cars.values.collect { |car| car.to_hash }
            track_hash   = @track.to_hash
            states_hash  = @states.collect { |state| state.to_hash }
            session_hash = Hash[ :laps => @laps,
                                 :maxLapTimeMs => @max_lap_time,
                                 :quickRace => @quick_race ]
            return Hash[ :cars => cars_hash,
                         :track => track_hash,
                         :raceSession => session_hash,
                         :states => states_hash ]
        end

        def to_compact_hash
            cars_hash    = @cars.values.collect { |car| car.to_hash }
            track_hash   = @track.to_hash
            states_hash  = @states.collect { |state| state.to_compact_hash }
            session_hash = Hash[ :laps => @laps,
                                 :maxLapTimeMs => @max_lap_time,
                                 :quickRace => @quick_race ]
            return Hash[ :cars => cars_hash,
                         :track => track_hash,
                         :raceSession => session_hash, :states => states_hash ]
        end

        def self.from_hash(hash)
            # Create cars
            cars_hash= hash['cars']
            cars = Hash.new
            cars_hash.each do |c|
                car = Car.from_hash(c)
                cars[c['id']] = car
            end
            track = Track.from_hash(hash['track'])
            session_hash = hash['raceSession']
            states_hash = hash['states']
            race = Race.new( cars,
                             track,
                             session_hash['laps'],
                             session_hash['maxLapTimeMs'],
                             session_hash['quickRace'] )
            if exists(states_hash)
                states_hash.each do |hash|
                    state = RaceState.from_hash(hash, race)
                    race.update_state(state)
                end
            end
            return race
        end
    end
end
