module ProjectD
    class RaceState
        attr_accessor :race, :tick, :car_states, :game_id
        
        def initialize(race, game_id, tick, car_states)
            @race = race
            @tick = tick
            @car_states = car_states
        end
        
        def clone
            clone = RaceState.new(@tick, @time)
            car_states = @car_positions.collect { |p| p.clone }
            clone.car_positions = car_positions
            return clone
        end
        
        def to_hash
            car_states_hash = []
            @car_states.each do |id, state|
                hash = state.to_hash
                hash[:id] = id
                car_states_hash.push(hash)
            end
            hash = Hash[ :gameId => @game_id,
                         :gameTick => @tick,
                         :carStates => car_states_hash]
        end

        def to_compact_hash
            car_states_hash = []
            @car_states.each do |id, state|
                hash = state.to_compact_hash
                hash[:a] = id
                car_states_hash.push(hash)
            end
            hash = Hash[ :a => @game_id,
                         :b => @tick,
                         :c => car_states_hash]
        end

        def self.from_hash(hash, race, game_id = nil, tick = nil)
            if hash.class == Hash
                if hash.key?('a') 
                    return from_compact_hash(hash, race)
                end
                game_id_tmp = hash['gameId']
                tick_tmp    = hash['gameTick']
                if exists(game_id_tmp) then game_id = game_id_tmp end
                if exists(tick_tmp) then tick = tick_tmp end
                car_states_hash = hash['carStates']
                car_states  = Hash.new
                car_states_hash.each do |c|
                    id = c['id']
                    car_states[id] = CarState.from_hash(c, race, race.cars[id])
                end
                state = RaceState.new(race, game_id, tick, car_states)
            else
                car_states = Hash.new
                hash.each do |c| 
                    id = c['id']
                    car_states[id] = CarState.from_hash(c, race, race.cars[id])
                end
                state = RaceState.new(race, game_id, tick, car_states)
            end
            return state
        end

        def self.from_compact_hash(hash, race, game_id = nil, tick = nil)
            game_id_tmp = hash['a']
            tick_tmp    = hash['b']
            if exists(game_id_tmp) then game_id = game_id_tmp end
            if exists(tick_tmp) then tick = tick_tmp end
            car_states_hash = hash['c']
            car_states = Hash.new
            car_states_hash.each do |c|
                id = c['a']
                car_states[id] = CarState.from_hash(c, race, race.cars[id])
            end
            state = RaceState.new(race, game_id, tick, car_states)
            return state
        end
    end

    class CarState
        attr_accessor :car, :piece, :piece_d, :lap, :start_lane, :end_lane,\
            :angle, :speed, :acceleration, :throttle, :crashed, :angle_speed,
            :angle_acceleration, :tan_speed, :tan_acceleration

        def initialize(car)
            @car         = car
            @piece       = nil
            @piece_d     = 0
            @lap         = 0
            @start_lane  = 0
            @end_lane    = 0
            @angle       = 0
            @speed       = 0
            @acceleration = 0
            @angle_speed = 0
            @angle_acceleration = 0
            @tan_speed   = 0
            @tan_acceleration = 0
            @crashed     = false
        end

        def clone
            clone = CarPosition.new
            clone.piece       = @piece
            clone.piece_d     = @piece_d
            clone.lap         = @lap
            clone.start_lane  = @start_lane
            clone.end_lane    = @end_lane
            clone.angle       = @angle
            clone.speed       = @speed
            clone.acceleration = @acceleration
            clone.throttle    = @throttle
            clone.crashed     = @crashed
            return clone
        end

        def to_hash
            lane_hash      = Hash[:startLaneIndex => @start_lane.index,
                                  :endLaneIndex => @end_lane.index]
            piece_pos_hash = Hash[:pieceIndex => @piece.index,
                                  :inPieceDistance => @piece_d,
                                  :lane => lane_hash]
            id_hash        = Hash[:name => @car.name,
                                  :color => @car.color]
            hash           = Hash[:id => id_hash,
                                  :angle => @angle,
                                  :piecePosition => piece_pos_hash,
                                  :lap => @lap ]
            if !@speed.nil? then        hash[:speed] = @speed end
            if !@acceleration.nil? then hash[:acceleration] = @acceleration end
            if !@throttle.nil? then     hash[:throttle] = @throttle end
            if !@crashed.nil? then      hash[:crashed] = @crashed end
            return hash
        end

        def to_compact_hash
            lane_hash      = Hash[:a => @start_lane.index,
                                  :b => @end_lane.index]
            piece_pos_hash = Hash[:a => @piece.index,
                                  :b => @piece_d,
                                  :c => lane_hash]
            id_hash        = Hash[:name => @car.name,
                                  :color => @car.color]
            hash           = Hash[:a => id_hash,
                                  :b => @angle,
                                  :c => piece_pos_hash,
                                  :d => @lap ]
            if !@speed.nil? then        hash[:e] = @speed end
            if !@acceleration.nil? then hash[:f] = @acceleration end
            if !@throttle.nil? then     hash[:g] = @throttle end
            if !@crashed.nil? then      hash[:h] = @crashed end
            return hash 
        end
        
        def self.from_hash(hash, race, car)
            if hash.key?('a') 
                return from_compact_hash(hash, race, car)
            end
            state = CarState.new(car)
            state.angle       = hash['angle']
            pieces = race.track.pieces
            piece_pos_hash    = hash['piecePosition']
            state.piece       = pieces[piece_pos_hash['pieceIndex']]
            state.piece_d     = piece_pos_hash['inPieceDistance']
            lanes = race.track.lanes
            lane_hash         = piece_pos_hash['lane']
            state.start_lane  = lanes[lane_hash['startLaneIndex']]
            state.end_lane    = lanes[lane_hash['endLaneIndex']]
            lap               = hash['lap']
            speed             = hash['speed']
            acceleration      = hash['acceleration']
            throttle          = hash['throttle']
            crashed           = hash['crashed']
            if !lap.nil? then          state.lap          = lap end
            if !speed.nil? then        state.speed        = speed end
            if !acceleration.nil? then state.acceleration = acceleration end
            if !throttle.nil? then     state.throttle     = throttle end
            if !crashed.nil? then      state.crashed      = crashed end
            return state
        end

        def self.from_compact_hash(hash, race, car)
            pieces = race.track.pieces
            piece_pos_hash = hash['c']
            lanes          = race.track.lanes
            lane_hash      = piece_pos_hash['c']
            speed          = hash['e']
            acceleration   = hash['f']
            throttle       = hash['g']
            crashed        = hash['h']
            state = CarState.new(car)
            state.angle       = hash['b']
            state.piece       = pieces[piece_pos_hash['a']]
            state.piece_d     = piece_pos_hash['b']
            state.start_lane  = lanes[lane_hash['a']]
            state.end_lane    = lanes[lane_hash['b']]
            state.lap         = hash['d']
            if speed.nil? then        state.speed        = speed end
            if acceleration.nil? then state.acceleration = acceleration end
            if throttle.nil? then     state.throttle     = throttle end
            if crashed.nil? then      state.crashed      = crashed end
            return state
        end
    end
end
