# encoding: utf-8

require 'socket'
require 'json'
require 'fileutils'
require_relative './globals'
require_relative './util'
require_relative './car'
require_relative './track'
require_relative './race'
require_relative './race_state'
require_relative './bot'


module ProjectD
  
    class Client
        attr_accessor :tcp, :bot

        # Initialize
        def initialize(bot_name, bot_key, server_host, server_port)
            @tcp            = TCPSocket.open(server_host, server_port)
            @bot = Bot.new(self)
            #create_race(bot_name, bot_key, 3, "keimola", "2wsxdsw2")
            #join_race(bot_name, bot_key, 3, "keimola")
            play(bot_name, bot_key)
        end
        

        # Join the server and start listening for messages
        # @param bot_name [String] name of the bot
        # @param bot_key [String] the bot key
        def play(bot_name, bot_key)
            puts_visual('Sending: play')
            send_tcp(join_message(bot_name, bot_key))
            listen_tcp()
        end

        def create_race( bot_name, bot_key, car_count = 1, track_name = nil,
                         password = nil )
            puts_visual('Sending: createrace')
            send_tcp(create_race_message( bot_name, bot_key, car_count,
                                          track_name, password ))
            listen_tcp()
        end

        def join_race( bot_name, bot_key, car_count = 1, track_name = nil,
                       password = nil)
            puts_visual('Sending: joinrace')
            send_tcp(join_race_message(bot_name, bot_key, car_count, track_name,\
                                       password))
            listen_tcp()
        end

        # Send message to throttle
        # @param value [Float] 0.0..1.0
        def throttle(value)
            #print_visual('throttle' + value.to_s)
            send_tcp(throttle_message(value))
        end

        # Send message to switch lane
        # @param dir [Integer] -1 for left, 1 for right 
        def switch_lane(dir)
            #print_visual('switch' + dir.to_s)
            send_tcp(switch_lane_message(dir))
        end
        
        def turbo
            #print_visual('throttle' + value.to_s)
            send_tcp(turbo_message)
        end

        def ping
            #print_visual(['▖''▗','▘','▙','▚','▛','▜','▝','▞','▟'].sample)
            send_tcp(ping_message)
        end


        private
        
        # Listen and process messages from the server until the connection
        # is terminated
        # @param tcp [TCPSocket] the socket to listen to
        def listen_tcp()
            while json = @tcp.gets do
                begin
                    
                    message = JSON.parse(json)
                    data = message['data']
                    case message['msgType']
                        when 'yourCar'
                            your_car(data)
                        when 'gameInit'
                            game_init(data)
                        when 'gameStart'
                            game_start(data)
                        when 'carPositions'
                            tick = message['gameTick']
                            tick = exists(tick) ? tick : 0 
                            car_positions(data, message['gameId'],\
                                          tick)
                        when 'crash'
                            crash(data, message['gameTick'])
                        when 'spawn'
                            spawn(data, message['gameTick'])
                        when 'lapFinished'
                            lap_finished(data)
                        when 'dnf'
                            dnf(data)
                        when 'turboAvailable'
                            turbo_available(data)
                        when 'turboStart'
                            turbo_start(data, message['gameTick'])
                        when 'turboEnd'
                            turbo_end(data, message['gameTick'])
                        when 'finish'
                            finish(data)
                        when 'gameEnd'
                            game_end('data')
                        when 'tournamentEnd'
                            tournament_end(data)
                    end

                rescue StandardError => error
                    puts "! ERROR: #{error}"
                    if DEBUG_MODE == true then raise end
                end
                
                begin
                    # ask bot to do something 
                    if !@bot.nil?
                        tick = message['gameTick']
                        @bot.act(tick)
                    end

                rescue StandardError => error
                    puts "! ERROR: #{error}"
                    if DEBUG_MODE == true then raise end
                end
            end
        end
       
        def your_car(data)
            print_visual('gotcar')
            @bot.car_id = data
        end

        # Init game with the data associated with server message type
        # 'gameInit'
        def game_init(data)
            puts_visual('Received: gameInit')
            race = Race.from_hash(data['race'])
            @bot.reset()
            @bot.race = race
        end
        
        def game_start(data)
            puts_visual('Received: gameStart')
            @bot.start_race
        end


        # Update car positions according to the data associated with server
        # message of type 'carPositions'
        # @param data data received with the message
        def car_positions(data, game_id, game_tick)
            #print_visual(['▖''▗','▘','▙','▚','▛','▜','▝','▞','▟'].sample)
            race_state = RaceState.from_hash(data, @bot.race, game_id, game_tick)
            bot.race.update_state(race_state)
        end
       
        def crash(data, game_tick)
            #print_visual('crash')
            @bot.crashed(data, game_tick)
        end

        def spawn(data, game_tick)
            #print_visual('spawn')
            @bot.spawned(data, game_tick)
        end

        def turbo_available(data)
            turbo = Turbo.from_hash(data)
            @bot.turbo_available(turbo)
        end

        def turbo_start(data, tick)
            @bot.turbo_start(data, tick)
        end

        def turbo_end(data, tick)
            @bot.turbo_end(data, tick)
        end
        
        def lap_finished(data)
            #print_visual('lapfinish')
            #TODO
        end
        
        def dnf(data)
           #TODO 
        end

        def finish(data)
           #TODO 
        end

        def game_end(data)
            puts_visual('Received: gameEnd')
            #race_log = Hash[ :race => @bot.race.to_compact_hash].to_json
            #log(race_log)
            @bot.reset
        end

        def tournament_end(data)
            print_visual('tournamentend')
            #TODO
        end


        # This will only send messages to the server. Wraps tcp.puts in case
        # we need to control messages sent somehow(In the hwo pong contest we
        # needed to limit message count to 5/s). 
        def send_tcp(msg)
            @tcp.puts(msg)
        end

        def join_message(bot_name, bot_key)
            data = Hash[:name => bot_name, :key => bot_key]
            msg  = Hash[:msgType => 'join', :data => data]
            return msg.to_json
        end

        def create_race_message( bot_name, bot_key, car_count, track_name = nil,\
                                 password = nil )
            bot_id = Hash[ :name => bot_name,
                           :key => bot_key ]
            data   = Hash[ :botId => bot_id, 
                           :carCount => car_count]
            if exists(track_name) then data[:trackName] = track_name end
            if exists(password)   then data[:password]  = password end
            msg = Hash[ :msgType => 'createRace',
                        :data => data ]
            return msg.to_json
        end

        def join_race_message( bot_name, bot_key, car_count,
                               track_name = nil, password = nil )
            bot_id = Hash[:name => bot_name, :key => bot_key]
            data   = Hash[:botId => bot_id,  :carCount => car_count]
            if exists(track_name) then data[:trackName] = track_name end
            if exists(password)   then data[:password]  = password end
            msg = Hash[:msgType => 'joinRace', :data => data]
            return msg.to_json
        end

        def throttle_message(value)
            msg = Hash[:msgType => 'throttle', :data => value]
            return msg.to_json
        end

        def switch_lane_message(dir)
            data = (dir == 1 ? "Right" : "Left")
            msg = Hash[:msgType => 'switchLane', :data => data]
            return msg.to_json
        end

        def turbo_message
            msg = Hash[:msgType => 'turbo', :data => '...']
            return msg.to_json
        end

        def ping_message
            return Hash[:msgType => 'ping'].to_json
        end
    end
end

#
#  Default bot config 
#
server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

bot_name    = exists(bot_name) ? bot_name       : 'Project.D'
bot_key     = exists(bot_key) ? bot_key         : 'JYR9y/3gXVA'
server_host = exists(server_host) ? server_host : 'testserver.helloworldopen.com'
server_port = exists(server_port) ? server_port : '8091'

puts_visual(
"
    ____     ____    ____        __    ______   ______  ______       ____ 
   / __ \\   / __ \\  / __ \\      / /   / ____/  / ____/ /_  __/      / __ \\
  / /_/ /  / /_/ / / / / / __  / /   / __/    / /       / /        / / / /
 / ____/  / _, _/ / /_/ / / /_/ /   / /___   / /___    / /     _  / /_/ / 
/_/      /_/ |_|  \\____/  \\____/   /_____/   \\____/   /_/     (_)/_____/  

  Bot Name: #{bot_name}
  Bot Key: #{bot_key}
  Server Host: #{server_host}:#{server_port}
  "
)

if DEBUG_MODE == true
  puts_visual("  Running in debug mode")
else
  puts_visual("  Running in production mode. The bot will do it's best to avoid crashing.")
end
puts_visual('')


#   Run
client = ProjectD::Client.new(bot_name, bot_key, server_host, server_port)

