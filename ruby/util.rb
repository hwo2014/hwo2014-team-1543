
# Check if value is not nil or empty string
def exists(value)
    !(value == '' || value.nil?)
end

# Puts text if visual is on
def puts_visual(str)
    if VISUAL then puts(str) end
end

# Print text if visual is on
def print_visual(str)
    if VISUAL then print(str) end
end

def puts_debug(str)
    if DEBUG_MODE then puts(str) end
end

# Print to log
def log(str)
    puts('')
    puts('LOG_BEGIN')
    if LOG then puts(str) end
    puts('LOG_END')
end

# Raise error if hash doesn't contain all the keyss in the given array
def check_keys(hash, keys)
    if !(keys.all? { |k| hash.key?(k) })
        raise 'Invalid hash'
    end
end

module ProjectD    

    # I have a feeling that this won't be used 
    # Get all raw logs as array of log strings(usually json strings)
    #def ProjectD.get_log_strings()
    #    dir = LOG_DIR;          
    #    puts_debug('Reading logs...')
    #    puts_debug('Log directory is' + dir)
    #    # Get log directory entries
    #    entries = Dir.entries(dir)
    #    puts_debug('Possible log files found: ' + (entries.length - 2).to_s)
    #    # Process all log directory files into one string
    #    files_str = ''
    #    entries.each do |entry|
    #        puts_debug('Processing ' + dir + entry)
    #        if File.directory?(LOG_DIR + '/' + entry) then next end
    #        file = File.open(LOG_DIR + '/' + entry, "r")
    #        files_str += file.read
    #        file.close
    #    end
    #    # Check each line of the files for LOG_BEGIN and LOG_END lines and capture
    #    # everything in between and add it to log_strings array
    #    log = false
    #    log_strings = []
    #    lines = files_str.lines
    #    lines.each do |l|
    #        log &= (!l.include?('LOG_END'))
    #        if log then log_strings.push(l) end
    #        log |= (l.include?('LOG_BEGIN'))
    #    end
    #    # Return log strings left after parsing and separating log and non-log
    #    # data from the files in log directory
    #    puts_debug(log_strings.length.to_s + ' lines of log data found')
    #    return log_strings
    #end
    
    
    # I have a feeling that this won't be used 
    # Return array of races reconstructed from logged data
    #def ProjectD.get_log_races()
    #    races = []
    #    log_strings = get_log_strings
    #    log_strings.each do |log_str|
    #        hash = JSON.parse(log_str)
    #        race_hash = hash['race']
    #        if exists(race_hash)
    #            races.push(Race.from_hash(race_hash))
    #        end
    #    end
    #    return races
    #end
    
    # Not in use anywhere
    #class LinearFunc
    #    attr_accessor :a, :b

    #    def initialize(a, b)
    #        @a = a
    #        @b = b
    #    end

    #    def getX(y)
    #        return (y - b) / a
    #    end

    #    def getY(x)
    #        return a * x + b
    #    end

    #    def self.approx(x_vals, y_vals)
    #        a_vals = []
    #        b_vals = []
    #        Range.new(0, x_vals.length - 2).step(2).each do |i|
    #            x0 = x_vals[i]
    #            x1 = x_vals[i + 1]
    #            y0 = y_vals[i]
    #            y1 = y_vals[i + 1]
    #            a_vals.push((y0 - y1) / (x0 - x1))
    #            b_vals.push((x0 * y1 - x1 * y0) / (x0 - x1))
    #        end
    #        a_avg = a_vals.inject { |sum, a| sum += a} / a_vals.length
    #        b_avg = b_vals.inject { |sum, b| sum += b} / b_vals.length
    #        return LinearFunc.new(a_avg, b_avg)
    #    end
    #end

    class AccelerationFunc
        attr_accessor :a, :b

        def initialize(a, b)
            @a = a
            @b = b
        end

        def getX(throttle, y)
            return (y - @b * throttle) / @a
        end

        def getY(throttle, x)
            return @a * x + @b * throttle
        end

        def speed_to_throttle(speed)
            throttle = - @a * speed / @b
        end

        def self.approx(x_vals, y_vals)
            a_vals = []
            b_vals = []
            Range.new(0, x_vals.length - 2).step(2).each do |i|
                x0 = x_vals[i]
                x1 = x_vals[i + 1]
                y0 = y_vals[i]
                y1 = y_vals[i + 1]
                a_vals.push((y0 - y1) / (x0 - x1))
                b_vals.push((x0 * y1 - x1 * y0) / (x0 - x1))
            end
            a_avg = a_vals.inject { |sum, a| sum += a} / a_vals.length
            b_avg = b_vals.inject { |sum, b| sum += b} / b_vals.length
            return AccelerationFunc.new(a_avg, b_avg)
        end
    end
    
    # Drifting algorithm unknown atm
    #class DriftAngleFunc
    #    attr_accessor :a, :b

    #    def initialize(a, b)
    #        @a = a
    #        @b = b
    #    end

    #    def getX(y)
    #        return y / (Math.cos(y / 180 * Math::PI ) * @a) - b
    #    end

    #    def getY(x)
    #        min = 0
    #        max = Math::PI / 2
    #        err = 1000.0
    #        x_rad = x / 180 * Math::PI
    #        x_result_rad = 0
    #        y_result_rad = 0
    #        while err >= 0.01
    #            step = (max - min) / 20
    #            Range.new(min, max).step(step).each do |y_test|
    #                x_test = [y_test / (Math.cos(y_test) * @a + @b), 0].max
    #                err_tmp = (x_rad - x_test).abs
    #                if err_tmp < err
    #                    x_result_rad = x_test
    #                    y_result_rad = y_test
    #                    err_tmp = err
    #                end
    #            end
    #            min = y_result_rad - step
    #            max = y_result_rad - step
    #        end
    #        return y_result_rad / Math::PI * 180
    #    end

    #    def self.approx(x_vals, y_vals)
    #        a_vals = []
    #        b_vals = []
    #        Range.new(0, x_vals.length - 2).step(2).each do |i|
    #            x0 = x_vals[i]
    #            x1 = x_vals[i + 1]
    #            y0 = y_vals[i]
    #            y1 = y_vals[i + 1]
    #            a_vals.push((y0 - y1) / (x0 * Math.cos(y0) - x1 * Math.cos(y1)))
    #            b_vals.push((y1 * x0 * Math.cos(y0) - y0 * x1 * Math.cos(y1)) /
    #                        (x0 * Math.cos(y0) - x1 * Math.cos(y1)))
    #        end
    #        a_avg = a_vals.inject { |sum, a| sum += a} / a_vals.length
    #        b_avg = b_vals.inject { |sum, b| sum += b} / b_vals.length
    #        return DriftAngleFunc.new(a_avg, b_avg)
    #    end
    #end
end

