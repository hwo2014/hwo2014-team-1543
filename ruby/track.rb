module ProjectD
  class Track
    attr_accessor :id, :name, :pieces, :lanes, :max_angle,
        :acceleration_func, :drift_angle_func, :max_angular_speed
    
    def initialize(id, name, pieces, lanes, starting_point)
        @id     = id
        @name   = name
        @pieces = pieces
        @lanes  = lanes
        @max_angular_speed = 2.5
    end
    
    def to_hash
        pieces_hash = @pieces.collect { |piece| piece.to_hash }
        lanes_hash  = @lanes.collect { |lane| lane.to_hash }
        hash = Hash[ :id => @id,
                     :name => @name,
                     :pieces => pieces_hash,
                     :lanes => lanes_hash,
                     :startingPoint => @starting_point ]
        return hash
    end

    def self.from_hash(hash)
        
        lanes_hash = hash['lanes']
        lanes = Array.new(lanes_hash.length)
        for l in lanes_hash
            lane = Lane.from_hash(l)
            lanes[lane.index] = lane
        end

        pieces_hash = hash['pieces']
        pieces = []
        pieces_hash.each_index do |i|
            piece = Piece.from_hash(i, lanes, pieces_hash[i])
            pieces.push(piece)
        end
        
        return Track.new(hash['id'], hash['name'], pieces, lanes,\
                         hash['startingPoint'])
    end
  end


  class Piece 
    attr_accessor :index, :switch, :bridge, :radius, :angle
    
    def initialize(index, switch, bridge)
        @index = index
        @switch = switch
        @bridge  = bridge
        @length = 0
        @radius = 0
        @angle = 0
    end

    def length(lane)
        return @length
    end
    
    def to_hash
        return Hash[:switch => @switch, :bridge => @bridge]
    end

    def self.from_hash(index, lanes, hash)
        radius  = hash['radius']
        angle   = hash['angle']
        length  = hash['length']
        switch  = hash['switch']
        bridge  = hash['bridge']
        piece = exists(radius) ? \
            BendPiece.new(index, radius, angle, switch, bridge, lanes) : \
            StraightPiece.new(index, length, switch, bridge)
        return piece
    end
  end


  class StraightPiece < Piece
        
        def initialize(index, length, switch, bridge)
            super(index, switch, bridge)
            @length = length
        end

        def to_hash
            hash = super
            hash[:length] = @length
            return hash
        end
  end


  class BendPiece < Piece
        attr_accessor :radius, :angle
        
        def initialize(index, radius, angle, switch, bridge, lanes)
            super(index, switch, bridge)
            @radius = radius
            @angle  = angle
            @angle_rad = Math::PI / 180 * angle
            @length = @angle_rad * radius
            dir = angle / angle.abs
            @lane_lengths = lanes.collect { |l| (radius - dir * l.offset) * @angle_rad.abs }
        end

        def length(lane)
            return @lane_lengths[lane.index]
        end
    
        def speed_to_angular(speed, lane)
            return speed / @lane_lengths[lane.index] * @angle
        end

        def angular_to_speed(angular, lane)
            return angular / @angle * @lane_lengths[lane.index]
        end
        
        def to_hash
            hash = super
            hash[:radius] = @radius
            hash[:angle] = @angle
            return hash
        end
  end


  class Lane
      attr_accessor :index, :offset
      
      def initialize(index, offset)
          @index = index
          @offset = offset
      end

      def to_hash
        return Hash[:index => @index, :distanceFromCenter => offset]
      end

      def self.from_hash(hash)
          return Lane.new(hash['index'], hash['distanceFromCenter'])
      end
  end
end
