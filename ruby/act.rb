require_relative 'globals'


module ProjectD


    class Act
        attr_accessor :bot, :act

        def initialize(bot)
            @bot = bot
            @act = lambda { act }
            @fin = false
        end

        def call
            return @act.call
        end
        
        def act
            bot.ping
        end

        def fin?
            return @fin
        end

        def self.available?(bot)
            return true
        end
    end
    
    class DefaultAct < Act
        attr_accessor :switch_sent
        def initialize(bot)
            super(bot)
            @switch_sent = false
        end

        def act
            bend_speed = 6

            race = @bot.race

            if race.nil? || race.states.empty?
                @bot.throttle(MAX_THROTTLE)
                return
            end
            
            track = race.track
            acc_func = track.acceleration_func
            car_id = @bot.car_id
            car    = race.cars[car_id]
            turbo  = car.turbo
            default_speed = acc_func.getX(DEFAULT_THROTTLE, 0)
            default_throttle = DEFAULT_THROTTLE
            car_state = race.states[-1].car_states[car_id]
            angle = car_state.angle
            pieces = track.pieces
            piece = car_state.piece
            next_piece = pieces[(piece.index + 1) % pieces.length]
            lanes = track.lanes

            if !turbo.nil?
                a = acc_func.a
                b = acc_func.b
                turbo_func = AccelerationFunc.new(a, b * turbo.factor)
                if turbo.fired
                    default_throttle = turbo_func.speed_to_throttle(default_speed)
                    acc_func = turbo_func
                end
            end

            if !piece.switch && !@switch_sent
                switches = []
                switches.push(race.next_switch(car_id))
                switches.push(race.next_switch(car_id, switches[0].index))
                lane_distances = lanes.collect { |l| race.distance_to_piece( \
                    car_id, l, switches[1], switches[0].index, \
                    0.5 * switches[0].length(l)) }
                
                shortest_i = nil
                shortest_d = 10000
                lane_distances.each_index do |i|
                    d = lane_distances[i]
                    if d < shortest_d
                        shortest_d = d
                        shortest_i = i
                    end
                end
                
                next_car_dist = race.next_car_distance(car_id)
                if (next_car_dist < 100)
                    current_lane_i = car_state.end_lane
                    if current_lane_i == 0
                        @bot.switch(1)
                        @switch_sent = true
                        return
                    else
                        @bot.switch(-1)
                        @switch_sent = true
                        return
                    end
                end
                
                current_lane_i = car_state.end_lane.index
                if shortest_i > current_lane_i
                    next_car_dist = race.next_car_distance(car_id, current_lane_i + 1)
                    if next_car_dist > 100
                        @bot.switch(1)
                        @switch_sent = true
                        return
                    end
                    @switch_sent = true
                elsif shortest_i < current_lane_i
                    next_car_dist = race.next_car_distance(car_id, current_lane_i - 1)
                    if next_car_dist > 100
                        @bot.switch(-1)
                        @switch_sent = true
                        return
                    end
                    @switch_sent = true
                else
                    @switch_sent = true
                end
            end

            if @switch_sent && piece.switch
                @switch_sent = false
            end
          
            if piece.class == StraightPiece ||
                    (piece.class == BendPiece && (angle / piece.angle > 0))
                next_bend = race.next_bend(car_id)
                next_bend_d = race.distance_to_next_bend(car_id,
                                                         car_state.end_lane)
                t = next_bend_d / ((car_state.speed + bend_speed) / 2)
                a = (bend_speed - car_state.speed) / t
                throttle = (a - acc_func.a * bend_speed) / acc_func.b
                if throttle < 0.3 then
                    @bot.throttle(throttle)
                elsif !car.turbo.nil? && !car.turbo.fired &&
                        next_piece.class == StraightPiece && angle < 0.01
                    @bot.turbo
                else
                    @bot.throttle(MAX_THROTTLE)
                end
                return
            end

            throttle = acc_func.speed_to_throttle(bend_speed)
            @bot.throttle(throttle)
        end

        def fin?()
            return false
        end

        def self.available(bot)
            return true
        end
    end

    class TestAccelerationAct < Act
        attr_accessor :count

        def initialize(bot)
            super(bot)
            @count = 0
        end

        def act
            @bot.throttle(ACCELERATION_TEST_THROTTLE)
            @count += 1
            @fin |= @count > 11
            race = @bot.race
            track = race.track
            car_id = @bot.car_id
            if @fin
                states = race.states.slice(-8, 7)
                car_states          = states.collect { |s| s.car_states[car_id] }
                acceleration_values = car_states.collect { |s| s.acceleration }
                speed_values = car_states.collect { |s| s.speed }
                acceleration_func = AccelerationFunc.approx( speed_values,
                                                       acceleration_values)
                @bot.race.update_track_acceleration_function(acceleration_func)
            end
        end

        def self.available(bot)
            race      = bot.race
            car_state = race.states[-1].car_states[bot.car_id]
            pieces    = race.track.pieces
            i  = car_state.piece.index
            i2 = (i + 1) % pieces.length
            return pieces[i].class == StraightPiece &&
                   pieces[i2].class == StraightPiece &&
                   car_state.speed <= ACCELERATION_TEST_MAX_START_SPEED
        end
    end
   
    # Friction is included in the acceleration test
    #class TestFrictionAct < Act
    #    attr_accessor :count

    #    def initialize(bot)
    #        super(bot)
    #        @count = 0
    #    end

    #    def act
    #        @bot.throttle(FRICTION_TEST_THROTTLE)
    #        @count += 1
    #        @fin |= @count > 11
    #        race = @bot.race
    #        track = race.track
    #        car_id = @bot.car_id
    #        if @fin
    #            states = race.states.slice(-8, 7)
    #            car_states          = states.collect { |s| s.car_states[car_id] }
    #            acceleration_values = car_states.collect { |s| s.acceleration}
    #            speed_values        = car_states.collect { |s| s.speed }
    #            friction_func = LinearFunc.approx( speed_values,
    #                                               acceleration_values)
    #            
    #            @bot.race.update_track_friction_function(friction_func)
    #        end
    #    end

    #    def self.available(bot)
    #        race      = bot.race
    #        car_state = race.states[-1].car_states[bot.car_id]
    #        pieces    = race.track.pieces
    #        i  = car_state.piece.index
    #        i2 = (i + 1) % pieces.length
    #        return pieces[i].class == StraightPiece &&
    #               pieces[i2].class == StraightPiece &&
    #               car_state.speed > FRICTION_TEST_MIN_START_SPEED
    #    end
    #end


    class TestMaxAngleAct < Act
        def initialize(bot)
            super(bot)
        end

        def act
            @bot.throttle(MAX_THROTTLE)
            race = @bot.race
            car_states = race.states[-1].car_states
            car_state = car_states[@bot.car_id]
            @fin |= car_state.crashed == true
        end

        def self.available(bot)
            race      = bot.race
            car_state = race.states[-1].car_states[bot.car_id]
            pieces    = race.track.pieces
            i  = car_state.piece.index
            i2 = (i + 1) % pieces.length
            return pieces[i].class == StraightPiece &&
                   pieces[i2].class == BendPiece
        end
    end

    class TestDriftAct < Act
        attr_accessor :count, :last, :angle_spd_vals, :angle_vals,
            :throttle_counts, :throttles, :current_throttle,
            :throttle_angles, :throttle_angular_speeds

        def initialize(bot)
            super(bot)
            @count = 0;
            @last = 0;
            @throttles = DRIFT_TEST_THROTTLE_VALUES
            @throttle_counts = @throttles.collect { |t| 0 }
            @throttle_angles = @throttles.collect { |t| 0 }
            @throttle_angular_speeds = @throttles.collect { |t| 0 }
            @current_throttle = 0
        end

        def act
            @bot.throttle(@throttles[@current_throttle])
            race = @bot.race
            track = race.track
            states     = [ race.states[-4],
                           race.states[-3],
                           race.states[-2] ]
            car_states = states.collect { |s| s.car_states[@bot.car_id]}
            angles     = car_states.collect { |s| s.angle.abs }
            if(angles[1] > angles[0] && angles[1] > angles[2] &&
               angles[1] > 10)
                angle      = angles[1]
                piece      = car_states[1].piece
                last_piece = car_states[0].piece
                speed      = car_states[1].speed
                lane       = car_states[1].start_lane
                if piece.class == BendPiece
                    angular_speed = piece.speed_to_angular(speed, lane).abs
                elsif last_piece.class == BendPiece
                    angular_speed = last_piece.speed_to_angular(speed, lane).abs
                else
                    return
                end
                prev_angle = @throttle_angles[current_throttle]
                if angle > prev_angle
                    @throttle_angles[@current_throttle] = angle
                    @throttle_angular_speeds[@current_throttle] = angular_speed
                end
                @throttle_counts[@current_throttle] += 1
            end
            if @throttle_counts[@current_throttle] >= 2
                @current_throttle += 1
            end
            if @current_throttle >= @throttles.length
                @fin |= true
            end
            if @fin
                puts_visual("---DRIFT---")
                puts_visual("angular speeds: " + @throttle_angular_speeds.to_json)
                puts_visual("angles: " + @throttle_angles.to_json)
                puts_visual("-----------")
            end
            # TODO: Just printing out numbers, need to figure out the
            # drifting algorithm
        end

        def self.available(bot)
            race      = bot.race
            car_state = race.states[-1].car_states[bot.car_id]
            pieces    = race.track.pieces
            i  = car_state.piece.index
            i2 = (i + 1) % pieces.length
            return pieces[i].class == StraightPiece &&
                   pieces[i2].class == BendPiece
        end
    end
end
