module ProjectD
    class Car
        attr_accessor :name, :color, :length, :width, :flag_pos, :throttle,
            :crashed, :turbo
        
        def initialize(name, color, length, width, flag_pos)
            @name   = name
            @color  = color
            @length = length
            @width  = width
            @flag_pos = flag_pos
            @crashed = false
            @throttle = 0.0
            @turbo = nil
            @max_angle = 0
        end

        def id_hash
            return Hash[ 'name' => name,
                         'color' => color ]
        end

        def to_hash
            dim_hash = Hash[ :length => @length,
                             :width => @width,\
                             :guideFlagPosition => @flag_pos ]
            return Hash[ :id => id_hash,
                         :dimensions => dim_hash ]
        end

        def self.from_hash(hash)
            id_hash  = hash['id']
            dim_hash = hash['dimensions']
            return Car.new(id_hash['name'], id_hash['color'],\
                           dim_hash['length'], dim_hash['width'],\
                           dim_hash['guideFlagPosition'])
        end
    end

    class Turbo
        attr_accessor :duration, :duration_ms, :factor, :start_tick, :fired
        
        def initialize(duration, duration_ms, factor)
            @duration = duration
            @duration_ms = duration_ms
            @factor = factor
            @start_tick = nil
            @fired = false
        end

        def self.from_hash(hash)
            duration = hash['TurboDurationTicks']
            duration_ms = hash['TurboDurationMilliseconds']
            factor = hash['turboFactor']
            turbo = Turbo.new(duration, duration_ms, factor)
        end
    end
end
