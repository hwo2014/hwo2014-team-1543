require 'set'
require_relative 'act'

module ProjectD 
    
    class Bot
        attr_accessor :client, :car_id, :race, :data_mode, :current_tick

        def initialize(client)
            @client = client
            @race   = nil 
            @car_id = nil
            @current_tick = 0
            @act = DefaultAct.new(self)
            @tests_to_run = TESTS.to_set
            puts_visual('Data gather mode on')
            @data_mode = true
        end

        def act(tick)
            if !tick.nil?
                @current_tick = tick
            end
            
            if @race.nil? || @race.states.empty?
                if @act.class != DefaultAct
                    @act = DefaultAct.new(self)
                end
                @act.call
                return
            end

            print_car_info

            if @data_mode
                puts_debug('Gather data')
                gather_data
                return
            else
                if @act.class != DefaultAct
                    @act = DefaultAct.new(self)
                end
            end
            
            @act.call
        end
        
        def start_race
            @action = lambda { throttle(DEFAULT_THROTTLE) }
        end

        def throttle(val)
            val = [[val, 1.0].min, 0.0].max
            @client.throttle(val)
            puts_debug("Throttle: " + val.to_s)
            if !@race.nil?
                @race.cars[@car_id].throttle = val
                if !@race.states.empty?
                    last_state = @race.states.last
                    last_car_state = last_state.car_states[@car_id]
                    if last_state.tick >= @current_tick
                        last_car_state.throttle = val
                    end
                end
            end
        end

        def switch(dir)
            @client.switch_lane(dir)
            puts_visual('Switched lane: ' + dir.to_s)
        end

        def turbo()
            @client.turbo
            puts_visual('Turbo used')
        end
        
        def crashed(car_id, tick)
            puts_visual(car_id.to_s + ' crashed')
            race.crashed(car_id, tick)
        end
        
        def spawned(car_id, tick)
            puts_visual(car_id.to_s + ' spawned')
            race.spawned(car_id, tick)
        end

        def turbo_available(t)
            puts_visual("Turbo available: " + t.to_json.to_s)
            @race.cars[@car_id].turbo = t
        end

        def turbo_start(car_id, tick)
            if car_id == @car_id
                puts_visual("Turbo start")
                car = @race.cars[car_id]
                car.turbo.fired = true
                car.turbo.start_tick = tick
            end
        end

        def turbo_end(car_id, tick)
            if car_id == @car_id
                puts_visual("Turbo end")
                car = @race.cars[car_id]
                car.turbo = nil
            end
        end

        def reset

        end

        private

        def gather_data
            if(TESTS.include?(@act.class))
                if @act.fin?
                    puts_debug('End test: ' + @act.class.to_s)
                    @tests_to_run.delete(@act.class)
                else
                    @act.call
                    return
                end
            end
            act = new_test
            if !act.nil?
                @act = act
                @act.call
                return
            else
                @act = DefaultAct.new(self)
            end
            @act.call
        end

        def new_test
            act = nil
            tests_available = TESTS.select { |t| t.available(self) }
            runnable_tests  = @tests_to_run.intersection(tests_available)
            if !runnable_tests.empty?
                act = runnable_tests.first.new(self)
                puts_visual('Begin new test: ' + act.class.to_s)
            end
            if @tests_to_run.empty?
                puts_debug('No more tests to run, turning data_mode off')
                @data_mode = false
            end
            return act
        end

        def print_car_info
            if @race.states.length > 1
                state = @race.states[-2]
                car_state = state.car_states[@car_id]
                if !car_state.nil?
                    puts_debug('-------------------')
                    puts_debug('Tick: ' + state.tick.to_s)
                    puts_debug('Car piece: ' + car_state.piece.index.to_s)
                    puts_debug('Car piece d: ' + car_state.piece_d.to_s)
                    puts_debug('Car velocity: ' + car_state.speed.to_s)
                    puts_debug('Car acceleration: ' + car_state.acceleration.to_s)
                    puts_debug('Car angle: ' + car_state.angle.to_s)
                    puts_debug('Car angle speed: ' + car_state.angle_speed.to_s)
                    puts_debug('Car angle acceleration: ' + car_state.angle_acceleration.to_s)
                    puts_debug('Car tan speed: ' + car_state.tan_speed.to_s)
                    puts_debug('Car tan acceleration: ' + car_state.tan_acceleration.to_s)
                    puts_debug('Car crashed: ' + car_state.crashed.to_s)
                    puts_debug('-------------------')
                end
            end
        end
    end
end
